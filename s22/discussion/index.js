// console.log("Hello World!");

// ==== Array Methods =====
// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items

// ====[SECTION]===== Mutator Methods
/*
	- This are methods that 'mutate' or change an array after they are created.
	- This methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];




//  ==== push() ==== ADDS AN ELEMENT AT THE END of an array AND returns the new array's length
	/*
		Syntax:
		arrayName.push(elementToBeAdded);
	*/



console.log("Current fruits array:");
console.log(fruits);


let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); //5 - returns the new array's length
console.log("Mutated array from push method:");
console.log(fruits);


// ADDING MULTIPLE ELEMENTS TO AN ARRAY ========


fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);



// ======pop() - REMOVE THE LAST ELEMENT in an array AND returns the removed element
	/*
		Syntax:
		arrayName.pop();
	*/



let removedFruit = fruits.pop();
console.log(removedFruit); // returns the removed element (Guava)

console.log("Mutated array from pop method:");
console.log(fruits);




// ======unshift() - ADDS ONE OR MORE ELEMENT at the beginning of an array AND 'returns the NEW array'
	/*
		Syntax:
		arrayName.unshift(elementA, elementB)
	*/



fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);



// ====== shift() - REMOVES AN ELEMENT at the beginning of an array AND returns the removed element
	/*
		Syntax:
		arrayName.shift();
	*/



let anotherFruit = fruits.shift();
console.log(anotherFruit); // Lime -returns the removed element (first element)
console.log("Mutated array from shift method:");
console.log(fruits);



//====== splice() - SIMULTANEOUSLY REMOVES AN ELEMENT from a specified index number AND adds elements AND returns the removed elements
	/*
		Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/



let fruitsSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitsSplice); // Apple and Orange - removed elements
console.log("Mutated array from splice method:");
console.log(fruits);




// ====== sort() - REARRANGE AN ARRAY into an ALPHANUMERIC ORDER
	/*
		syntax:
		arrayName.sort();
	*/



fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);




// ====== reverse() - REVERSES THE ORDER of the array elements
	/*
		syntax:
		arrayName.reverse();
	*/



fruits.reverse();
console.log("Mutated array from sort method:");
console.log(fruits);




// ====== Non-Mutator Methods - 
	/*
		- non-mutator methods are methods that do not modify or change an array after they're created. 
		-These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing outputs
	*/



let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);



// ====== indexof() - returns the index number of the first matching element found in an array
				/*
					-The search process will start from the first element proceeding to the last element
					-If no match was found it will return -1(negative 1)

					Syntax:
					arrayName.indexOf(searchValue);
					arrayName.indexOf(searchValue, fromIndex);
				*/



let firstIndex = countries.indexOf("PH"); // countries.indexOf("PH", 2);  2 - indicates 2nd value of the same element
console.log("Result of indexOf method: " + firstIndex); // 1

let invalidCounty = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCounty); // -1 returns negative one if value not found in the element




// ====== lastIndexOf() - RETURNS THE INDEX NUMBER of the last matching element found in the array
				/*
					- The search process will be done from the last element proceeding to the first element

					Syntax:
					arrayName.indexOf(searchValue);
				*/



let lastIndex = countries.lastIndexOf("PH"); // countries.lastIndexOf("PH", 4);   4 - from right to left starting from last element indicated (PH)
console.log("Result of lastIndexOf method: " + lastIndex);




// ====== slice() - Portions/slices elements from an array AND returns the new/sliced array
				/*
					Syntax:
					arrayName.slice(startingIndex);
				*/



let sliceArrayA = countries.slice(2); // 2 - indicates slicing elements starting from a specified index
console.log("Result of slice method:");
console.log(sliceArrayA); // ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

let slicedArrayB = countries.slice(2,5);  // slicing off elements starting from a specific index to another index (index 2,3,4) not including the 5th index
console.log("Result of slice method:");
console.log(slicedArrayB); // ['CAN', 'SG', 'TH']

let slicedArrayC = countries.slice(-3); // slicing off elements starting from the last element of an array(last 3 elements)
console.log("Result of slice method:");
console.log(slicedArrayC); // ['PH', 'FR', 'DE']





// ====== toString() - returns an array as a string separated by commas




let stringArray = countries.toString(); // returns an array as a string separated by commas
console.log("Results from toString method: ")
console.log(stringArray); // US,PH,CAN,SG,TH,PH,FR,DE




// ====== concat() - combines two arrays and returns the combined result/array
				/*
					Syntax:
					arrayName.concat(arrayB);
				*/



let taskArrayA = ["drink html", "eat javascript"]
let taskArrayB = ["inhale css", "breath bootstrap"]
let taskArrayC = ["get git", "be node"]

let tasks = taskArrayA.concat(taskArrayB); // combines two arrays and returns the combined result/array
console.log("Results from concat method: ")
console.log(tasks); // ['drink html', 'eat javascript', 'inhale css', 'breath bootstrap']



// Combining multiple arrays
console.log("Results from concat method: ")
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining arrays with specific elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Results from concat method: ")
console.log(combinedTasks);





// ====== join() - RETURNS an array as a string separated by a specified separator
				/*
					Syntax:
					arrayName.join("separator");
				*/




let users = ["John", "Jane", "Joe", "Joshua"]
console.log(users.join()) // default separator(comma)
console.log(users.join(" ")) // whitespace separator
console.log(users.join(" - ")) // dash separator





//         [SECTION] Iterator METHODS


// ====== forEach()
				/*
					- similar to a for loop that iterates on each array element
					- For each item in the array, the anonymous function passed in the forEach() will be run
					- The anonymous function is able to receive the current item being iterated or looped over by assigning a parameter
					- does not return anything

					Syntax:
						arrayName.forEach(function(individualElement) {
						statement
						})
				*/



let filteredTasks = [];

allTasks.forEach(function(task) {

	if (task.length > 10) {
		filteredTasks.push(task);
	}
})
console.log("Result of forEach method: ")
console.log(filteredTasks);




// ====== map()
			/*
				-iterates on each element and RETURNS a new array

				Syntax:
					arrayName.map(function(individualElement){
						statement;
					})
			*/




let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
})
console.log("Original Array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);


// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
})
console.log(numberForEach); // undefined - beacuse forEach() does not return anything





// ====== every() - results to boolean 
			/*
				- checks if all elements in an array meet the given condition
				- RETURNS a true value if all elements meet the condition and false if otherwise
			*/



let allValid = numbers.every(function(number){
	return number < 3;
})
console.log("Result of every method: ");
console.log(allValid);





// ====== some() - results to boolean
			/*
				- Checks if AT LEAST ONE ELEMENT in the array MEETS THE GIVEN CONDITION
				- RETURNS a true value if some elements meet the condition and false if otherwise
			*/




let someValid = numbers.some(function(number) {
	return number < 2;
})
console.log("Result of every method: ");
console.log(someValid);





// ====== filter()
			/*
				- RETURNS a new array that contains the elements which meet the given condition
				- RETURNS an empty array if no elements where found
			*/




let filterValid = numbers.filter(function(number){
	return number < 3;
})
console.log("Result of filter method: ");
console.log(filterValid);





// ====== includes()
				/*
					- Checks if the argument passed can be found in the array 
					- It RETURNS a boolean value
				*/




let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);




// ====== reduce()
			/*
				- Evaluates elements from left to right and returns/reduces the array into a single value
			*/




let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {

	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;

})
console.log("Results of reduce method: " + reducedArray);



let list = ["Hello", "Again", "World"];

// 1. x = hello, y = again => Hello Again + World
// 2. x = hello world + y = again => Hello Again World
let reducedString = list.reduce(function(x, y) {
	return x + " " + y;
})
console.log("Result of reduce method: " + reducedString); // Result of reduce method: Hello Again World