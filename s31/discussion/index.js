// Use the "require" directive to load the Node.js modules
//  A module is a software component or part of a program that contains one or more routines/functions
// "http" is a Node JS module that lets Node JS transfer data using Hyper Text Transfer Protocol
// Provides a functionality for creating HTTP servers and clients, allowing applications to send and receive HTTP request
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client
// The messages sent by the client, usually a web browser, are called "request".
// The messages sent by the server as answer/s are called "responses"

// The server will be assigned to port 4000 via "listen(4000)" method where the server will listen to any requests that are sent to it
// A port is a virtual point where network connections start and end
http.createServer(function (request, response) {
	// Use the writeHead method() method to:
		/*
			1. Set a Status code for the response - 200 means OK
			2. Set the content-type of the response as a plain text message
		*/
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content "Hello World!"
	response.end("Hello World!")

}).listen(4000);

// When server is running, console will print the message:
console.log("Server is running at localhost:4000");
