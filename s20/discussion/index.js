// console.log("Hello World!")

// [SECTION] While Loop
	// A while loop takes in an expression/condition
	// If the condition evaluates to true, the statement/code block inside will be executed
/*
	Syntax:
	while(expression/condition) {
	statement
	}
*/

let count = 5;

// While the value of "count" is not equal to 0
while(count !== 0) {

	// The current value of "count" is printed out
	console.log("While: " + count);

	// Decreases the value of count by 1 after every iteration
	// Iteration is the term given to the repition of statement
	count--;
}


// [SECTION] Do-while loop
	// A do-while loop works a lot like the while loop, but unlike the while look, do-while loop guarantee that the code will be executed once	

/*
	Syntax:
	do {
		statement/code block
	} while (expression/condition)
*/

/*
	How does the do-while loop work?
		1. The statement in the "do" block executes once
		2. The message "Do while:" + number will be printed out in the console
		3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given condition
		4. If the condition is met/true, another iteration of the loop will be executed
		5. If the expression/condition is false, the loop will stop 
*/

let  number = Number(prompt("Give me a number."));

do {
	console.log("Do while: " + number)

	// Increases the value of "number" by 1 after every iteration to stop the loop when it reaches 10 or greater
	number+= 1;
} while (number < 10)




// [SECTION] For loop 
	// - A for loop is more flexible than while and do-while. It consist of 3 parts.
		// 1. The "initialization" value that will track the progression of the loop
		// 2. The "expression/condition" that will evaluate if the loop will run one more time
		// 3. The "finalExpression" indicates how to advance the loop
/*
	Syntax:
	for (initialization; expression/condition; finalEpression) {
	statement;
	}
*/

for (let count = 0; count <=20; count++) {
	console.log(count);
}

for ( let count = 50; count>=0; count--) {
	console.log(count);
}

let myString = "alex";
// Characters in strings may be counted using the .length property
console.log(myString.length);

// Accessing elements of a string
// Individual characters of a string may be accessed using its index number
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// Will create a loop that will print out the individual letters of "myString" variable
					// i < 4
for(let i = 0; i < myString.length; i++) {
	console.log(myString[i])
}


let myName = "AlEx"; 

// Creates a loop that will print out the letter of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
/*
	- How this For Loop works:
	        1. The loop will start at 0 for the the value of "i"
	        2. It will check if "i" is less than the length of myName (e.g. 0)
	        3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
	        4. If the expression/condition is true the console will print the number 3.
	        5. If the letter is not a vowel the console will print the letter
	        6. The value of "i" will be incremented by 1 (e.g. i = 1)
	        7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
*/
for (let i = 0; i < myName.length; i++){

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	) {
		console.log(3);
	} else {
		console.log(myName[i])
	}
}


// [SECTION] Continue and Break Statements

for (let count = 0; count <=20; count++) {

	// If remainder is equal to "0"
	if (count % 2 === 0) {
		// Tells the code to continue to the next iteration of the loop
		// This also ignores all statements located after the continue statement
		continue;
	}
	// The current value of count is printed out if the remainder is not equal to '0'
	console.log("Continue and Break: " + count);

	// if the current value of "count" is greater than 10
	if(count>10){

		// Tells the code to terminate the loop even if the condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		break;
	}
}

let name = "alexandra";
				// 'i' < 9
for(let i = 0; i < name.length; i++) {

	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}
	if (name[i] === "d") {
		break;
	}
}