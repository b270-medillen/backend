// console.log("Hello World!");

//Functions
	//Functions in JS are lines/blocks of code that can tell our device/application to perform a certain task when called or invoked.

// Function Declarations
	// function statement - defines a function with a specified parameter


	/* Syntax:
		function functionName() {
			code block (statement)
		}

		- function keyword - used to define a JS function
		-functionName - is the function's name. Functions are named to be able to be used or called later in our code.
		-function block ({}) - the statements which comprise the body of the function. This is where the code to be executed is written.

	*/

	function printName() {
		console.log("Hi! My name is John.");
	};

	// Function invocation.
	printName();

	// declaredFunction(); - results to an error ('Uncaught ReferenceError: declaredFunction is not defined')

	// Semicolons are used to separate executable JS statements or codes.


// Function Declaration vs Function Expression


	// 1. Function declaration
	declaredFunction(); //declared functions can be hoisted. As long as the function has been declared.

	function declaredFunction() {
		console.log("Hello World from declaredFunction()");
	};

	declaredFunction();


	// 2. Function Expression
		// A function can also be stored in a variable.
		// A function expression is anonymous function assigned to the variableFunction.

		/*
			let variableName = function() {
				code block(statement);
			}
		*/
		// let n = 30; // - thi sis how we initialize a variable

		// variableFunction(); -  this will result to an error

		let variableFunction = function() {
			console.log("Hello again Batch270!");
		}

		variableFunction();

		let funcExpression = function funcName() {
			console.log("Hello from the other side.");
		}

		funcExpression();

		// You can reassign declared functions and function expressions to a new anonymous functions.

		declaredFunction = function() {
			console.log("updated delcaredFunction");
		}

		funcExpression = function() {
			console.log("updated funcExpression");
		}

		declaredFunction();
		funcExpression();

		const constantFunction = function() {
			console.log("Initialized with const");
		}
		constantFunction();
		declaredFunction();


		/*constantFunc = function() {
			console.log("Cannot be re-assigned");
		} -  this will result to an error. ('Uncaught TypeError: Assignment to constant variable.')

		constantFunc();*/


// Function Scoping

	/*
		Scope is the accessibility (visibility) of a variables within our program.

			JavaScript Variables has 3 types of scope:
				1. local/block scope
				2. global scope
				3. function scope

	*/


	// 1. Local Scope
		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

	// 2. Global Scope

		let globalVar = "Mr. World";

		console.log(globalVar);
		//console.log(localVar);

	// 3. Function Scope

		function showNames() {
			// function scoped variables
			var functionVar = "Joe"
			const functionConst = "John"
			let functionLet = "Jane"

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}

		showNames();

			//error
			//console.log(functionVar);
			//console.log(functionConst);



// 4. Nested Function 
		// we can create another function inside a function. This is called a nested function, this nested function, being inside the myNewFunction will have access to the variable name, as they are within the same scope/code block


		function myNewFunction() {
			let name = "Jane";

			function nestedFunction() {
				let nestedName = "John";
				console.log(name);	
				console.log(nestedName);
			}
			
			nestedFunction();	
		}
		// nestedFunction(); results to an error
		myNewFunction();


		// Function and Global Scope Variables

		let globalName = "Alexander";

		function myNewFunction2() {
			let nameInside = "Renz";

			console.log(globalName);
		}
		myNewFunction2();
		// console.log(nameInside); results to an error


// Using alert()

		// Syntax: alert("messageInString")
		// alert("Hello World!"); // This will run immediately when the page loads

		function showSampleAlert() {
			alert("Hello, User!");
		}
		// showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed.");


// Using Prompt()

		// Syntax: prompt("dialogInString");

		// let samplePrompt = prompt("Enter your name.");
		// console.log("Hello, " + samplePrompt);
		// console.log(typeof samplePrompt);

		// let sampleNullPrompt = prompt("Don't enter anything.");
		// console.log(sampleNullPrompt); // resturns an empty string when there is no input or null if the user cancel the prompt.

		function printWelcomeMessage() {
			let firstName = prompt("Enter your first name.");
			let lastName = prompt("Enter your last name.");

			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}
		printWelcomeMessage();


// Function Naming Conventions
		//Function names should be descriptive of the task that it will perform. It will usually contains a verb.

		function getCourses() {
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}
		getCourses();

		// avoid generic names to avoid confusion within your code.

		function get(){
			let name = "Jamie";
			console.log(name);
		}
		get();

		// avoid pointless and inappropriate function names

		function foo(){
			console.log(25%5);
		}
		foo();

		// name your functions using camelCasing

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}
		displayCarInfo();